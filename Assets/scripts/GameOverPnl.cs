﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverPnl : MonoBehaviour
{
    public Button _btn;

    void Start()
    {
        _btn.onClick.AddListener(() => Controller.Instance.LoadSceneAsync(SceneType.splash));    
    }

    void OnDestroy()
    {
        _btn.onClick.RemoveAllListeners();
    }
}
