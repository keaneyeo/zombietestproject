﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    [SerializeField] Transform _player;
    public float CamSpeed = 2f;
    float _sensitivity = 0.7f;
    Vector3 _direction;
    float _distanceFromPlayer = 0;
    InputHandler _handler;
    float _topDownY = 30;
    Quaternion _topDownRotation = Quaternion.Euler(55, 0, 0);
    float _ZDistance = 20;

    float _backDistance = 7f;
    float _backViewY = 4;
    float _currentX;
    float _currentY;
    float _minY = 20;
    float _maxY = -10;

    void Start()
    {
        _handler = InputHandler.Instance;
        Vector3 delta = this.transform.position - _player.position;
        _direction = delta.normalized;
        _distanceFromPlayer = delta.magnitude;
    }

    void Update()
    {
        if (_handler.ControlMode == ControlMode.BackView)
        {
            Vector3 dir = _handler.GetShootingDirection();
            float speed = _handler.GetShootingSpeed();
            _currentY -= dir.z * _sensitivity * speed;
            _currentX += dir.x * speed;
            _currentY = Mathf.Clamp(_currentY, _maxY, _minY);
        }
        else
        {
            _currentY = 0;
            _currentX = 0;
        }
    }

    void LateUpdate()
    {
        if(_handler.ControlMode == ControlMode.TopDown)
        {
            Vector3 offsetpos = _player.transform.position + new Vector3(2, _topDownY, -_ZDistance);
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, _topDownRotation, 3f * Time.deltaTime);
            this.transform.position = Vector3.Lerp(this.transform.position, offsetpos, CamSpeed * Time.deltaTime);
        }
        else
        {
            Vector3 offsetpos = _player.transform.position + new Vector3(0, _backViewY, 0);
            Vector3 dir = new Vector3(0, 0, -_backDistance);
            Quaternion rotation = Quaternion.Euler(_currentY, _currentX, 0);
            Vector3 targetpos = offsetpos + rotation * dir;
            this.transform.position = targetpos;
            this.transform.LookAt(offsetpos);
        }
    }
}
