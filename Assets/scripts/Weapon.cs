﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public int WeaponType;
    public float FireDelay;
    public int Damage;
    public GameObject Bullet;
    public int BaseBulletCount;
    public float ReloadTime;
    int _currentBulletCount;
    public int CurrentBulletCount => _currentBulletCount;
    float _timer = 0;
    bool _canShoot = true;
    public Transform BulletSpawnPos;

    void Start()
    {
        _currentBulletCount = BaseBulletCount;
    }

    public bool CheckClip()
    {
        return _currentBulletCount > 0;
    }

    public void Shoot()
    {
        if (_canShoot)
        {
            Fire();
            _canShoot = false;
            _timer = FireDelay;
        }
    }

    void Update()
    {
        _timer -= Time.deltaTime;
        if(_timer <= 0 && !_canShoot)
        {
            _canShoot = true;
            _timer = FireDelay;
        }
    }

    void Fire()
    {
        Bullet b = Instantiate(Bullet, BulletSpawnPos.position, Quaternion.identity).GetComponent<Bullet>();
        b.Init(30, Damage, this.transform.forward);
        _currentBulletCount--;
    }

    public void Reload()
    {
        _currentBulletCount = BaseBulletCount;
    }
}
