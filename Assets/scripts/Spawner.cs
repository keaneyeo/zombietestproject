﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] Spawners;
    public GameObject BossSpawn;

    public bool CheckPlayerNearby(GameObject spawner)
    {
        Collider[] cols = Physics.OverlapSphere(spawner.transform.position, 5f, 1 << LayerMask.NameToLayer("Player"));
        if (cols.Length != 0)
            return true;

        return false;
    }

    public GameObject GetSpawner()
    {
        int random = Random.Range(0, Spawners.Length);
        return Spawners[random];
    }
}
