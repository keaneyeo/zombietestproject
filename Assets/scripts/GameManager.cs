﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region singleton
    static GameManager _instance;
    public static GameManager Instance => _instance;

    void Awake()
    {
        _instance = this;
    }

    void OnDestroy()
    {
        _instance = null;
    }
    #endregion

    public GameObject[] NormalZombie;
    public GameObject ClownZombie;
    public GameObject BossZombie;
    public Spawner Spawner;
    public Transform Player;
    public GameObject GameOverPnl;
    public GameObject WinPnl;
    public Transform Canvas;

    List<WaveData> _waveData = new List<WaveData>();
    List<SpecialWaveData> _specialWaveData = new List<SpecialWaveData>();
    WaveData _currentWaveData;
    int _currentWave;
    int _currentSpecialWave;
    int _currentCombinedWaves;
    float _spawnTimer;
    int _currentZombieCount;
    int _totalZombiesSpawned;
    int _totalDefeatedZombies;
    int _totalZombiesInWave;
    bool _startSpawn = false;
    bool _specialWave = false;
    int[] _normalHitpoints = new int[] { 100, 125, 150, 175 };
    int _totalCombinedWaves = 5;
    bool _bossWave = false;
    bool _bossSpawned = false;
    int _bossZombieCount;

    public bool GameOver => _gameOver;
    bool _gameOver;

    void Start()
    {
        GenerateWaveData();
        _spawnTimer = 5f;
        _startSpawn = true;
        _currentWave = -1;
        _currentSpecialWave = -1;
        _totalCombinedWaves = _waveData.Count + _specialWaveData.Count;
        _currentCombinedWaves = 0;
        StartNewWave();
    }

    void StartNewWave()
    {
        Debug.Log("Start normal wave");
        _currentWave++;
        _currentWaveData = _waveData[_currentWave];
        _totalZombiesInWave = _currentWaveData.AmountOfZombiesToSpawn;
        _startSpawn = true;
        _spawnTimer = _currentWaveData.SpawnDelay;
        if (_currentWaveData.BossCount != 0)
        {
            _bossWave = true;
            _bossZombieCount = 9;
        }
    }

    void Update()
    {
        if (!_startSpawn || _gameOver)
            return;
        _spawnTimer -= Time.deltaTime;
        if (_spawnTimer <= 0)
        {
            GameObject spawner = Spawner.GetSpawner();
            if (Spawner.CheckPlayerNearby(spawner))
            {
                _spawnTimer = 0.1f; //if player is nearby, wait 0.1f then attempt to get another spawner
                return;
            }
            if (_currentZombieCount < _currentWaveData.MaxZombieAmount)
            {
                SpawnNormalZombie(_currentWaveData.HitPointBonus, spawner);
                _currentZombieCount++;
                _totalZombiesSpawned++;
                _spawnTimer = _currentWaveData.SpawnDelay;
                if (_totalZombiesSpawned >= _currentWaveData.AmountOfZombiesToSpawn && !_bossWave)
                {
                    _startSpawn = false;
                    _specialWave = true;
                }
            }
            else
            {
                _spawnTimer = 0.2f; //wait for .2 seconds if player killed a zombie before attempting to spawn another
            }
        }
    }

    void CheckBossSpawn()
    {
        if(_bossWave)
        {
            if(_totalDefeatedZombies >= 20 && !_bossSpawned)
            {
                SantaAI boss = Instantiate(BossZombie, Spawner.BossSpawn.transform.position, Quaternion.identity).GetComponent<SantaAI>();
                boss.Init(BossPhase.PhaseOne, 6, 11, 2500, 2.5f, 2);
                boss.SetTarget(Player);
                _bossSpawned = true;
            }
        }
    }

    public void BossZombieSpawned()
    {
        _currentZombieCount++;
    }

    public void BossDead()
    {
        _bossZombieCount--;
        if (_bossZombieCount == 0)
            _startSpawn = false;
    }

    public void ZombieDead()
    {
        _currentZombieCount--;
        _totalDefeatedZombies++;
        CheckBossSpawn();
        if (_totalDefeatedZombies >= _totalZombiesInWave && !_bossWave)
        {
            if (_specialWave)
                StartSpecialWave();
            else
                StartNewWave();
            _currentCombinedWaves++;
            _totalDefeatedZombies = 0;
            _currentZombieCount = 0;
            _totalZombiesSpawned = 0;
        }
        else if (_bossWave)
        {
            if(_bossZombieCount == 0 && _totalDefeatedZombies == _totalZombiesSpawned)
            {
                _gameOver = true;
                Instantiate(WinPnl, Canvas.transform);
                return;
            }
        }
    }

    void StartSpecialWave()
    {
        Debug.Log("Start special wave");
        _totalZombiesInWave = 0;
        _currentSpecialWave++;
        if (_currentSpecialWave >= _specialWaveData.Count)
            return;

        SpecialWaveData swd = _specialWaveData[_currentSpecialWave];
        GameObject prev = null;
        foreach(SpecialSpawn ss in swd.Spawns)
        {
            GameObject spawner = Spawner.GetSpawner();
            while(spawner == prev)
                spawner = Spawner.GetSpawner();
            switch (ss.SpawnType)
            {
                case SpawnType.Normal:
                    for (int i = 0; i < ss.SpawnAmount[0]; i++)
                    {
                        SpawnNormalZombie(0, spawner);
                        _totalZombiesInWave++;
                    }
                    break;
                case SpawnType.Special:
                    for (int i = 0; i < ss.SpawnAmount[0]; i++)
                    {
                        SpawnClownZombie(spawner);
                        _totalZombiesInWave++;
                    }
                    break;
                case SpawnType.Mixed:
                    for (int i = 0; i < ss.SpawnAmount.Length; i++)
                    {
                        for (int j = 0; j < ss.SpawnAmount[i]; j++)
                        {
                            if (i == 0)
                                SpawnNormalZombie(0, spawner);
                            else
                                SpawnClownZombie(spawner);
                            _totalZombiesInWave++;
                        }
                    }
                    break;
            }
            prev = spawner;
        }
        _specialWave = false;
    }

    void SpawnNormalZombie(int bonushp, GameObject spawner)
    {
        int random = Random.Range(0, NormalZombie.Length);
        ZombieAI zombie = Instantiate(NormalZombie[random], spawner.transform.position, Quaternion.identity).GetComponent<ZombieAI>();
        random = Random.Range(0, _normalHitpoints.Length);
        zombie.Init(_normalHitpoints[random] + bonushp);
        zombie.SetTarget(Player);
    }

    void SpawnClownZombie(GameObject spawner)
    {
        ClownAI clown = Instantiate(ClownZombie, spawner.transform.position, Quaternion.identity).GetComponent<ClownAI>();
        clown.Init(250);
        clown.SetTarget(Player);
    }

    public void EndGame()
    {
        Instantiate(GameOverPnl, Canvas.transform);
        _gameOver = true;
    }

    void GenerateWaveData()
    {
        WaveData wd = new WaveData()
        {
            MaxZombieAmount = 10,
            AmountOfZombiesToSpawn = 10,
            HitPointBonus = 0,
            BossCount = 0,
            SpawnDelay = 3,
        };
        _waveData.Add(wd);

        wd = new WaveData()
        {
            MaxZombieAmount = 10,
            AmountOfZombiesToSpawn = 15,
            HitPointBonus = 50,
            BossCount = 0,
            SpawnDelay = 2,
        };
        _waveData.Add(wd);

        wd = new WaveData()
        {
            MaxZombieAmount = 15,
            AmountOfZombiesToSpawn = 20,
            HitPointBonus = 100,
            BossCount = 1,
            SpawnDelay = 3,
        };
        _waveData.Add(wd);

        SpecialWaveData swd = new SpecialWaveData()
        {
            Spawns = new SpecialSpawn[3]
            {
                new SpecialSpawn()
                {
                    SpawnType = SpawnType.Normal,
                    SpawnAmount = new int[1] { 5 }
                },
                new SpecialSpawn()
                {
                    SpawnType = SpawnType.Normal,
                    SpawnAmount = new int[1] { 5 }
                },
                new SpecialSpawn()
                {
                    SpawnType = SpawnType.Special,
                    SpawnAmount = new int[1] { 1 }
                },
            }
        };
        _specialWaveData.Add(swd);

        swd = new SpecialWaveData()
        {
            Spawns = new SpecialSpawn[3]
            {
            new SpecialSpawn()
                {
                    SpawnType = SpawnType.Normal,
                    SpawnAmount = new int[1] { 5 }
                },
                new SpecialSpawn()
                {
                    SpawnType = SpawnType.Mixed,
                    SpawnAmount = new int[2] { 5, 1 }
                },
                new SpecialSpawn()
                {
                    SpawnType = SpawnType.Special,
                    SpawnAmount = new int[1] { 2 }
                },
            }
        };
        _specialWaveData.Add(swd);
    }
}

public class WaveData
{
    public int MaxZombieAmount;
    public int AmountOfZombiesToSpawn;
    public int HitPointBonus;
    public int BossCount;
    public float SpawnDelay;
}

public class SpecialWaveData
{
    public SpecialSpawn[] Spawns;
}

//honestly i feel there might be a better way but its a prototype
public class SpecialSpawn
{
    public SpawnType SpawnType;
    public int[] SpawnAmount;
}

public enum SpawnType
{
    Normal,
    Special,
    Mixed,
}