﻿using UnityEngine;

public interface IHealthHandler
{
    int Health { get; }
    void DoDamage(int damage);
    bool SendHitLocation(Vector3 pos);
}
