﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour
{
    static Controller _instance;
    public static Controller Instance => _instance;
    void Awake()
    {
        DontDestroyOnLoad(this);
        _instance = this;
    }

    void OnDestroy()
    {
        _instance = null;
    }

    void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0)
            SceneManager.LoadScene((int)SceneType.splash);
    }

    public void LoadSceneAsync(SceneType type)
    {
        StartCoroutine(LoadSceneAsyncI(type));
    }

    IEnumerator LoadSceneAsyncI(SceneType type)
    {
        AsyncOperation ao = SceneManager.LoadSceneAsync((int)type);
        float delay = 1f;
        while (!ao.isDone || delay > 0)
        {
            delay -= Time.deltaTime;
            //loading bar or animation or whatever goes here
            yield return null;
        }
        yield return null;
    }
}

public enum SceneType
{
    preload,
    splash,
    game,
}
