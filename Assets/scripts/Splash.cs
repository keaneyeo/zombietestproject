﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Splash : MonoBehaviour
{
    public Button Button;

    void Start()
    {
        Button.onClick.AddListener(() => Controller.Instance.LoadSceneAsync(SceneType.game));
    }

    void OnDestroy()
    {
        Button.onClick.RemoveAllListeners();
    }

}
