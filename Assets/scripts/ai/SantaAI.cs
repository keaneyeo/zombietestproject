﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SantaAI : ZombieAI
{
    public GameObject SantaPrefab;
    public BossPhase BossPhase;
    public int SplitAmount;

    public void Init(BossPhase phase, float attackrange, float movespeed, int health, float scale, int split)
    {
        BossPhase = phase;
        AttackDistance = attackrange;
        MoveSpeed = movespeed;
        _health = health;
        SplitAmount = split;
        HeadOffset *= scale;
        this.transform.localScale = new Vector3(scale, scale, scale);
        GameManager.Instance.BossZombieSpawned();
    }

    public override void Start()
    {
        base.Start();
        RegisterState(new SantaSplit(this));
    }

    public override void DoDamage(int damage)
    {
        _health -= damage;
        if (_health <= 0)
        {
            switch (BossPhase)
            {
                case BossPhase.PhaseOne:
                    for (int i = 0; i < SplitAmount; i++)
                    {
                        SantaAI santa = Instantiate(SantaPrefab, this.transform.position, Quaternion.identity).GetComponent<SantaAI>();
                        santa.SetTarget(Target);
                        santa.Init(BossPhase.PhaseTwo, 4, 8, 1600, 1.8f, 3);
                    }
                    Destroy(this.gameObject);
                    break;
                case BossPhase.PhaseTwo:
                    for (int i = 0; i < SplitAmount; i++)
                    {
                        SantaAI santa = Instantiate(SantaPrefab, this.transform.position, Quaternion.identity).GetComponent<SantaAI>();
                        santa.SetTarget(Target);
                        santa.Init(BossPhase.PhaseThree, 2, 5, 850, 1f, 0);
                    }
                    Destroy(this.gameObject);
                    break;
                case BossPhase.PhaseThree:
                    ChangeState(StateType.Ded, true);
                    break;
            }
            GameManager.Instance.BossDead();
        }
    }
}

public class SantaSplit : BaseState<SantaAI>
{
    public SantaSplit(SantaAI brain) : base(brain)
    {
    }

    public override StateType Type => StateType.SantaSplit;

    public override void OnStateEnter(object[] args)
    {
    }

    public override void OnStateExit()
    {
    }

    public override void OnStateUpdate()
    {
    }
}

public enum BossPhase
{
    PhaseOne,
    PhaseTwo,
    PhaseThree
}