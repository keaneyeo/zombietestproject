﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IBrainFSM : MonoBehaviour
{
    StateType _currentState;
    public StateType CurrentState => _currentState;
    IState _existing;
    Dictionary<StateType, IState> _states = new Dictionary<StateType, IState>();


    public void RegisterState(IState state)
    {
        _states[state.Type] = state;
    }

    public bool ChangeState(StateType type, params object[] args)
    {
        if (type == _currentState)
            return false;

        IState newState = null;
        if (!_states.TryGetValue(type, out newState))
            return false;
        
        _existing?.OnStateExit();
        newState.OnStateEnter(args);

        _currentState = type;
        _existing = newState;
        return true;
    }

    public void UpdateBrain()
    {
        _existing?.OnStateUpdate();
    }
}

public abstract class BaseState<T> : IState where T : IBrainFSM
{
    public T Machine;
    public BaseState(T brain) 
    {
        Machine = brain;
    }
}

public abstract class IState
{
    public IBrainFSM Brain;
    public abstract StateType Type { get; }
    public abstract void OnStateEnter(object[] args);
    public abstract void OnStateUpdate();
    public abstract void OnStateExit();
}

public enum StateType
{
    Idle,
    Run,

    Attack,
    RunToExplode,
    Ded,

    SantaSplit,
}