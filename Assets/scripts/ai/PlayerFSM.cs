﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerFSM : IBrainFSM, IHealthHandler
{
    public Rigidbody Rigid;
    public Animator Anim;
    public float MoveSpeed = 45;
    public float RotateSpeed = 2;
    public int WeaponType = 2; //using instructions from the asset pack
    public float HeadHorizontal = -1; //slightly tilted
    public float BodyVertical = 0.3f; //for run 
    public float BodyHorizontal = 0.6f;
    Camera MainCam;
    public Weapon CurrentWeapon;
    public float CurrentSpeedFactor;
    InputHandler _handler;

    //usually i would put this on a ui manager or a dedicated script but its a prototype
    public Text AmmoText;
    public Text Lives;

    bool _running = false;
    bool _reloading = false;

    int _health = 5;
    public int Health => _health;
    int _maxhealth;

    void Start()
    {
        _handler = InputHandler.Instance;
        _maxhealth = _health = 5;
        CurrentWeapon = GetComponentInChildren<Weapon>();//temp
        MainCam = Camera.main;
        Rigid = this.GetComponent<Rigidbody>();
        Anim = this.GetComponent<Animator>();
        RegisterState(new PlayerIdle(this));
        RegisterState(new PlayerRun(this));
        RegisterState(new PlayerDed(this));
        ChangeState(StateType.Idle);
        AmmoText.text = "Ammo: " + CurrentWeapon.BaseBulletCount + "/" + CurrentWeapon.BaseBulletCount;
        Lives.text = "Lives " + Health + "/" + Health;
    }

    void Update()
    {
        if (GameManager.Instance.GameOver)
            return;

        UpdateBrain();
        Vector3 dir = _handler.GetMovementDirection();
        dir.y = 0;
        float speed = _handler.GetMovementSpeed();
        //float x = Input.GetAxis("Horizontal");
        //float y = Input.GetAxis("Vertical");
        if (_handler.ControlMode == ControlMode.TopDown)
        {
            if (speed != 0 || dir != Vector3.zero)
            {
                CurrentSpeedFactor = speed;
                Vector3 targetpos = this.transform.position + dir * CurrentSpeedFactor * MoveSpeed * Time.deltaTime;
                Rigid.MovePosition(targetpos);
                if (!_running)
                {
                    this.ChangeState(StateType.Run);
                    _running = true;
                }
            }
            else
            {
                this.ChangeState(StateType.Idle);
                _running = false;
            }

            if (_handler.Shooting)
            {
                Shoot();
                Vector3 direct = _handler.GetShootingDirection();
                Quaternion lookdir = Quaternion.LookRotation(direct);
                this.transform.rotation = lookdir;
            }
        }
        else
        {
            if (speed != 0 || dir != Vector3.zero)
            {
                CurrentSpeedFactor = speed;
                Camera main = Camera.main;
                Vector3 camfor = main.transform.forward;
                Vector3 camr = main.transform.right;
                Vector3 forward = new Vector3(camfor.x * dir.z, 0, camfor.z * dir.z);
                Vector3 sideways = new Vector3(camr.x * dir.x, 0, camr.z * dir.x);
                Vector3 cameradir = (forward + sideways).normalized;
                Vector3 targetpos =  this.transform.position + cameradir * CurrentSpeedFactor * MoveSpeed * Time.deltaTime;
                Rigid.MovePosition(targetpos);
                if (!_running)
                {
                    this.ChangeState(StateType.Run);
                    _running = true;
                }
            }
            else
            {
                this.ChangeState(StateType.Idle);
                _running = false;
            }

            if (_handler.Shooting)
            {
                Shoot();
                Ray ray = Camera.main.ScreenPointToRay(new Vector2(Screen.width / 2, Screen.height / 2));
                Quaternion lookdir = Quaternion.LookRotation(ray.direction);
                this.transform.rotation = lookdir;
            }
        }

        if (Input.GetButtonDown("Reload"))
        {
            Reload();
        }

    }

    void Shoot()
    {
        if (_reloading)
            return;

        if (CurrentWeapon.CheckClip())
        {
            CurrentWeapon.Shoot();
            AmmoText.text = "Ammo: " + CurrentWeapon.CurrentBulletCount + "/" + CurrentWeapon.BaseBulletCount;
        }
        else
        {
            Reload();
        }
    }

    void Reload()
    {
        Anim.SetBool("Reload_b", true);
        StartCoroutine(ReloadDelay());
    }

    IEnumerator ReloadDelay()
    {
        _reloading = true;
        //I would rather use an animation event but the animatmion clips are read only
        yield return new WaitForSeconds(0.05f); //just to ensure the bool will trigger animation
        Anim.SetBool("Reload_b", false);
        float delay = CurrentWeapon.ReloadTime;
        while(delay >= 0)
        {
            delay -= Time.deltaTime;
            yield return null;
        }
        CurrentWeapon.Reload();
        _reloading = false;
        AmmoText.text = "Ammo: " + CurrentWeapon.BaseBulletCount + "/" + CurrentWeapon.BaseBulletCount;
        yield return null;
    }

    public void DoDamage(int damage)
    {
        _health--;
        Lives.text = "Lives " + _health + "/" + _maxhealth;
        if (_health <= 0 && !GameManager.Instance.GameOver)
        {
            //Game Over
            ChangeState(StateType.Ded);
            GameManager.Instance.EndGame();
        }
    }

    public bool SendHitLocation(Vector3 pos)
    {
        return false;
    }
}

public class PlayerIdle : BaseState<PlayerFSM>
{
    public PlayerIdle(PlayerFSM brain) : base(brain)
    {
    }

    public override StateType Type => StateType.Idle;

    public override void OnStateEnter(object[] args)
    {
        Machine.Anim.SetFloat("Speed_f", 0);
        Machine.Anim.SetFloat("Body_Vertical_f", 0);//not the best, causes abit of a weird transition
    }

    public override void OnStateExit()
    {
    }

    public override void OnStateUpdate()
    {
    }
}

public class PlayerRun : BaseState<PlayerFSM>
{
    float speed = 1;
    float delay = 0.3f;
    public PlayerRun(PlayerFSM brain) : base(brain)
    {
    }

    public override StateType Type => StateType.Run;

    public override void OnStateEnter(object[] args)
    {
        Machine.Anim.SetFloat("Speed_f", speed);
        
    }

    public override void OnStateExit()
    {
        Machine.Anim.SetFloat("Speed_f", 0);
    }

    public override void OnStateUpdate()
    {
        float speed = Mathf.Min(Machine.CurrentSpeedFactor, 0.3f);
        Machine.Anim.SetFloat("Body_Vertical_f", speed);
    }
}

public class PlayerDed : BaseState<PlayerFSM>
{
    public PlayerDed(PlayerFSM brain) : base(brain)
    {
    }

    public override StateType Type => StateType.Ded;

    public override void OnStateEnter(object[] args)
    {
        Machine.Anim.SetBool("Death_b", true);
    }

    public override void OnStateExit()
    {
        throw new System.NotImplementedException();
    }

    public override void OnStateUpdate()
    {
        throw new System.NotImplementedException();
    }
}