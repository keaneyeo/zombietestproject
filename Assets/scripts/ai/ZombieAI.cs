﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieAI : IBrainFSM, IHealthHandler
{
    public NavMeshAgent Agent;
    public Animator Anim;
    public float MoveSpeed;
    public Transform Target;
    public float AttackDistance = 0.4f;
    public float ExtraAttackRange;
    public float HeadOffset = 2f;

    protected int _health = 100;
    public int Health => _health;

    public virtual void Init(int health)
    {
        _health = health;
    }

    public virtual void Start()
    {
        ExtraAttackRange = AttackDistance + 0.5f;
        Agent = this.GetComponent<NavMeshAgent>();
        Agent.speed = MoveSpeed;
        Anim = this.GetComponent<Animator>();
        RegisterState(new ZombieWalk(this));
        RegisterState(new ZombieAttack(this));
        RegisterState(new ZombieDed(this));
        ChangeState(StateType.Run);
    }

    public virtual void Update()
    {
        UpdateBrain();
    }

    public void Attack()
    {
        Vector3 delta = Target.position - this.transform.position;
        Collider[] cols = Physics.OverlapSphere(this.transform.position + delta.normalized * 1.5f + Vector3.up * 2, 1, 1 << LayerMask.NameToLayer("Player"));
        if(cols.Length != 0)
        {
            GameObject go = cols[0].gameObject;
            IHealthHandler handler = go.GetComponent<IHealthHandler>();
            Vector3 distance = this.transform.position - go.transform.position;
            if(distance.magnitude <= ExtraAttackRange)
                handler?.DoDamage(1);
        }
    }

    public virtual void DoDamage(int damage)
    {
        _health -= damage;
        if (_health <= 0)
        {
            ChangeState(StateType.Ded);
        }
    }

    public void SetTarget(Transform player)
    {
        Target = player;
    }

    /// <summary>
    /// To prevent bullets from hitting dead body
    /// </summary>
    public void DisableCollider()
    {
        this.GetComponent<Collider>().enabled = false;
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(this.transform.position + Vector3.up * 2.5f, 0.5f);
    }

    public virtual bool SendHitLocation(Vector3 pos)
    {
        float y = pos.y;
        if (y >= HeadOffset)
            return true;
        else
            return false;
    }
}

public class ZombieWalk : BaseState<ZombieAI>
{
    Transform target;
    public ZombieWalk(ZombieAI brain) : base(brain)
    {
    }

    public override StateType Type => StateType.Run;

    public override void OnStateEnter(object[] args)
    {
        target = Machine.Target;
        //Machine.Agent.SetDestination(target.position);
    }

    public override void OnStateExit()
    {
    }

    public override void OnStateUpdate()
    {
        Machine.Agent.SetDestination(target.position);
        float distance = Vector3.Distance(target.position, Machine.transform.position);
        if(distance <= Machine.AttackDistance)
        {
            Machine.Agent.SetDestination(Machine.transform.position);//to stop the agent
            Machine.Agent.isStopped = true;
            Machine.ChangeState(StateType.Attack);
        }
    }
}

public class ZombieAttack : BaseState<ZombieAI>
{
    float timer = 0f; //animation delay
    float damagedelay = 0.7f;
    bool attack = false;
    //again i would prefer to use animation event but its read only so this is a workaround
    
    public ZombieAttack(ZombieAI brain) : base(brain)
    {
    }

    public override StateType Type => StateType.Attack;

    public override void OnStateEnter(object[] args)
    {
        //Machine.Anim.SetTrigger("Attack");
    }

    public override void OnStateExit()
    {
        Machine.Agent.isStopped = false;
        attack = false;
        timer = 0;
        damagedelay = 0.7f;
    }

    public override void OnStateUpdate()
    {
        timer -= Time.deltaTime;
        if(timer <= 0 && !attack)
        {
            Machine.Anim.SetTrigger("Attack");
            timer = 2f;
            attack = true;
            //attempt attack
        }

        if (attack)
        {
            damagedelay -= Time.deltaTime;
            if(damagedelay <= 0)
            {
                Machine.Attack();
                attack = false;
                damagedelay = 0.7f;
            }
        }

        float distance = Vector3.Distance(Machine.Target.position, Machine.transform.position);
        if (distance >= Machine.ExtraAttackRange + 1f)
        {
            Machine.ChangeState(StateType.Run);
        }
    }
}

public class ZombieDed : BaseState<ZombieAI>
{
    public ZombieDed(ZombieAI brain) : base(brain)
    {
    }

    public override StateType Type => StateType.Ded;

    public override void OnStateEnter(object[] args)
    {
        bool boss = false;
        if(args.Length != 0)
        {
            boss = (bool)args[0];
        }
        Machine.Agent.SetDestination(Machine.transform.position);
        Machine.Agent.isStopped = true;
        int random = Random.Range(0, 2);
        Machine.Anim.SetInteger("Die", random);
        Object.Destroy(Machine.gameObject, 2f);
        if(!boss)
            GameManager.Instance.ZombieDead();
        Machine.DisableCollider();
    }

    public override void OnStateExit()
    {
    }

    public override void OnStateUpdate()
    {
    }
}