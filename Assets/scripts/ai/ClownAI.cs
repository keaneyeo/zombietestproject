﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClownAI : ZombieAI  //expanding on base zombie ai
{
    public float ExplodeRadius = 2f;

    public override void Start()
    {
        RegisterState(new ClownExplode(this));
        base.Start();
    }


    public override void DoDamage(int damage)
    {
        _health -= damage;
        if (_health <= 0)
        {
            ChangeState(StateType.RunToExplode);
        }
    }

    public void Explode()
    {
        Collider[] cols = Physics.OverlapSphere(this.transform.position, ExplodeRadius);
        if(cols.Length != 0)
        {
            foreach(Collider col in cols)
            {
                IHealthHandler handler = col.GetComponent<IHealthHandler>();
                handler?.DoDamage(125);
            }
        }
        //instantiate explosion
        DisableCollider();
        GameManager.Instance.ZombieDead();
        Destroy(this.gameObject);
    }
}

public class ClownExplode : BaseState<ClownAI>
{
    float timer = 3f;
    public ClownExplode(ClownAI brain) : base(brain)
    {
    }

    public override StateType Type => StateType.RunToExplode;

    public override void OnStateEnter(object[] args)
    {
        float speed = Machine.Agent.speed;
        Machine.Agent.speed = speed * 2;
    }

    public override void OnStateExit()
    {
        throw new System.NotImplementedException();
    }

    public override void OnStateUpdate()
    {
        Machine.Agent.SetDestination(Machine.Target.position);
        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            //Explode!
            Machine.Explode();
        }
    }
}