﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputHandler : MonoBehaviour
{
    static InputHandler _instance;
    public static InputHandler Instance => _instance;
    void Awake()
    {
        _instance = this;
    }
    void OnDestroy()
    {
        _instance = null;
        CameraButton.onClick.RemoveAllListeners();
    }

    public ControlMode ControlMode = ControlMode.TopDown;
    public MovementPad MovementPad;
    public MovementPad ShootingPad;
    public Button CameraButton;

    bool _moving = false;
    bool _shooting = false;
    public bool Shooting => _shooting;
    float _maxDistance = 2f;
    float _maxHeight = 300f; //max pixels from top to spawn

    void Start()
    {
        CameraButton.onClick.AddListener(ChangeMode);    
    }

    void Update()
    {
        if (GameManager.Instance.GameOver)
            return;

        if(Input.touchCount != 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                if (i > 1) //prevent taking input from more than 2 touches
                    continue;

                Touch touch = Input.GetTouch(i);
                Vector3 touchpos = touch.position;
                if (touchpos.y > (float)Screen.height - _maxHeight)
                    return;

                bool left = CheckSide(touchpos);
                if (left)
                {
                    if (!_moving)
                    {
                        _moving = true;
                        MovementPad.transform.position = touchpos;
                        MovementPad.SetStartPos(touchpos);
                    }
                    else
                    {
                        MovementPad.UpdatePos(touchpos);
                    }
                }
                else
                {
                    if (!_shooting)
                    {
                        _shooting = true;
                        ShootingPad.transform.position = touchpos;
                        ShootingPad.SetStartPos(touchpos);
                    }
                    else
                    {
                        ShootingPad.UpdatePos(touchpos);
                    }
                }

                if(touch.phase == TouchPhase.Ended)
                {
                    if (left)
                    {
                        _moving = false;
                        MovementPad.TurnOffPad();
                    }
                    else
                    {
                        _shooting = false;
                        ShootingPad.TurnOffPad();
                    }
                }
            }
        }
        //if (Input.GetButton("Fire1"))
        //{
        //    Vector3 mousepos = Input.mousePosition;
        //    bool left = CheckSide(mousepos);
        //    if (left)
        //    {
        //        if (!_moving)
        //        {
        //            _moving = true;
        //            MovementPad.transform.position = mousepos;
        //            MovementPad.SetStartPos(mousepos);
        //        }
        //        else
        //        {
        //            MovementPad.UpdatePos(mousepos);
        //        }
        //    }
        //    else
        //    {
        //        if (!_shooting)
        //        {
        //            _shooting = true;
        //            ShootingPad.transform.position = mousepos;
        //            ShootingPad.SetStartPos(mousepos);
        //        }
        //        else
        //        {
        //            ShootingPad.UpdatePos(mousepos);
        //        }
        //    }
        //}

        //if (Input.GetButtonUp("Fire1"))
        //{
        //    _moving = false;
        //    MovementPad.TurnOffPad();
        //    _shooting = false;
        //    ShootingPad.TurnOffPad();
        //}
    }

    void ChangeMode()
    {
        if (ControlMode == ControlMode.TopDown)
            ControlMode = ControlMode.BackView;
        else
            ControlMode = ControlMode.TopDown;
    }

    public Vector3 GetMovementDirection()
    {
        return MovementPad.GetDirection();
    }

    public float GetMovementSpeed()
    {
        return MovementPad.GetSpeed();
    }

    public Vector3 GetShootingDirection()
    {
        return ShootingPad.GetDirection();
    }

    public float GetShootingSpeed()
    {
        return ShootingPad.GetSpeed();
    }

    /// <summary>
    /// left is true, right is false
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    bool CheckSide(Vector3 pos)
    {
        float width = (float)Screen.width;
        float posx = pos.x;
        if (pos.x < width * 0.5f)
            return true;
        else
            return false;
    }
}

public enum ControlMode
{
    TopDown,
    BackView
}