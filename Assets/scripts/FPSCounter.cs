﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour
{
    public Text FpsCounter;
    float _updateTimer = 0.1f;

    
    void Update()
    {
        _updateTimer -= Time.deltaTime;
        if(_updateTimer <= 0)
        {
            float current = (int)(1f / Time.unscaledDeltaTime);
            FpsCounter.text = "fps: " + current.ToString();
            _updateTimer = 0.1f;
        }
    }
}
