﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPad : MonoBehaviour
{
    public GameObject Knob;
    float _maxDistance = 100f;
    Vector3 _startPos;
    Vector3 _dir;
    float _distance = 0;
    CanvasGroup _cg;

    private void Start()
    {
        _cg = GetComponent<CanvasGroup>();
        _cg.alpha = 0;
    }

    public void SetStartPos(Vector3 start)
    {
        _startPos = start;
        _cg.alpha = 1;
    }

    public void UpdatePos(Vector3 currentpos)
    {
        Vector3 delta = currentpos - _startPos;
        _dir = delta.normalized;
        _distance = Mathf.Min(delta.magnitude, _maxDistance);
        Knob.transform.position = _startPos + _dir * _distance;
    }

    public Vector3 GetDirection()
    {
        //translate y to z
        return new Vector3(_dir.x, 0, _dir.y);
    }

    public float GetSpeed()
    {
        return (_distance / _maxDistance);
    }

    public void TurnOffPad()
    {
        _dir = Vector3.zero;
        _distance = 0;
        Knob.transform.position = _startPos;
        _cg.alpha = 0;
    }
}
