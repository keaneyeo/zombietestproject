﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flashing : MonoBehaviour
{
    [SerializeField]
    [Range(0, 1)]
    float _maxAlpha = 1;
    [SerializeField]
    Graphic _targetGraphic;
    Graphic _graphic;

    void Start()
    {
        if (_targetGraphic == null)
            _graphic = GetComponent<Graphic>();
        _graphic = _targetGraphic == null ? GetComponent<Graphic>() : _targetGraphic;
    }

    void Update()
    {
        Color color = _graphic.color;
        color.a = Mathf.PingPong(Time.time * _maxAlpha, _maxAlpha);
        _graphic.color = color;
    }
}