﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    float _movespeed;
    int _damage;
    Vector3 _dir;
    float _duration = 10f;

    public void Init(float movespeed, int damage, Vector3 forward)
    {
        _damage = damage;
        _movespeed = movespeed;
        _dir = forward;
        Quaternion lookdir = Quaternion.LookRotation(_dir);
        this.transform.rotation = lookdir;
    }

    void Update()
    {
        _duration -= Time.deltaTime;
        if(_duration <= 0)
        {
            Destroy(this.gameObject);
        }
        this.transform.position += _movespeed * _dir * Time.deltaTime;
    }

    void OnTriggerEnter(Collider col)
    {
        //instantiate particle
        IHealthHandler handler = col.GetComponent<IHealthHandler>();
        if(handler != null)
        {
            if (handler.SendHitLocation(this.transform.position))
            {
                Debug.Log("Headshot!");
                handler.DoDamage(_damage * 2);
            }
            else
                handler.DoDamage(_damage);

        }
        //health handler
        Destroy(this.gameObject);
    }
}
